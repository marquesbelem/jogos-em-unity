# Jogos em Unity

Executáveis dos jogos que desenvolvi  

TapForddie: jogo de click ação desenvolvido para TCC da faculdade, onde o objetivo é proteger o crescimento 
da árvore principal.

I'm the Lucky One: jogo de click de escolha desenvolvido para aula de Produção de jogos da faculdade, objetivo é acertar qual
porta está o dinheiro.

Pingoal: Um jogo desenvolvido em parceria com ACE - Arts & Craft of Entertainment, levado para expor na bgs 2017,
totalmente programado por mim. É um mistrua de pinball com air rock, com personalização e pontos para usar em habilidades. 